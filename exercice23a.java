//Constante Tableaux
	static int Stocks[][]= {{1,56,200,55}, 
			{2,42,200,60}, {3,62,200,125}, {4,45,200,150},
			{5,25,200,140}, {6,72,200,86}, {7,40,200,47},
			{8,48,200,80}, {9,24 ,150,126},{10,36,200,164},
			{11,15,100,85},{12,25,80,23}};
	
	static String Names[]= {"Spa Reine","Bru Plate","Bru légèrement pétillante",
			"Pepsi", "Spa Orange","Schweppes Tonic","Schweppes Agrumes",
			"Lipton Ice Tea","Lipton Ice Tea Pêche","Jus d'orange Looza",
			"Cécémel","Red Bull"};
	
	//Constantes du programmes
	static public String ESP = " ";
	static int align[] = {35, 40 ,45}; // on établie un tableau d'écart entre valeurs
	
	
	public static void main(String[] args) {
		
		displayStocks();
		
	}

	public static String placeNumberRank(String strOriginalStr, String strNumberToPlace, int rank) {

		int x = rank - strOriginalStr.length() - strNumberToPlace.length() ;
		String modifiedString = strOriginalStr;

		for(int i = 0 ; i < x ; i ++) {
			modifiedString = modifiedString.concat(" ");
		}

		modifiedString = modifiedString.concat(strNumberToPlace);

		return modifiedString;
	}

	public static void displayStocks() {

		String chaine = " " ;
		
		for (int i = 0 ; i < Names.length - 1 ; i ++) {
			
			chaine = Names[i];
			
			for (int j = 1 ; j < Stocks[i].length ; j ++) {
				
				chaine = placeNumberRank(chaine, Integer.toString(Stocks[i][j]), align[j-1]);
				
			}	
			System.out.println(chaine);	
			
		}

	}